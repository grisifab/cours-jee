package org.eclipse.dao;

import java.sql.*;
import org.eclipse.models.*;

public class PersonneDAOImpl implements DAO<Personne> {
	
	@Override
	public Personne save(Personne personne) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				c.setAutoCommit(false);
				PreparedStatement ps = c.prepareStatement("insert into personne (nom,prenom) values (?,?); ",
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, personne.getNom());
				ps.setString(2, personne.getPrenom());
				ps.executeUpdate();
				c.commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return personne;
	}
}
