package org.eclipse.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import org.eclipse.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.models.Personne;

/**W
 * Servlet implementation class TestServlet
 */
@WebServlet("/ajoutPersonne")
public class ajoutPersonne extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutPersonne.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		out.println("hello 1");
		String nom = request.getParameter("nom");
		out.println("hello 2");
		String prenom = request.getParameter("prenom");
		out.println("hello 3");
		Personne personne = new Personne(nom,prenom);
		out.println("hello 4");
		PersonneDAOImpl daop = new PersonneDAOImpl();
		out.println("hello 5");
		Personne insertedPersonne = daop.save(personne);
		request.setAttribute("personne", insertedPersonne);
		this.getServletContext().getRequestDispatcher("/WEB-INF/confirmation.jsp").forward(request, response);
	}

}
