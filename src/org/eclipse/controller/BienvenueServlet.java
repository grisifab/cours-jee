package org.eclipse.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.models.Personne;

/**W
 * Servlet implementation class TestServlet
 */
@WebServlet("/bienvenue")
public class BienvenueServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static int comptage = 0;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		comptage ++;
		java.util.Date date = new java.util.Date();
		String cDate = String.valueOf(date);
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<meta charset=\"utf-8\"/>");
		out.println("<title>BienvenueServlet</title>");
		out.println("</head>");
		out.println("<body>");
		out.println(" bienvenur au cours de programmation");
		out.println("<br>" + cDate);
		out.println(" <br>nombre de visites : " + comptage);
		out.println("</body>");
		out.println("</html>");

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
